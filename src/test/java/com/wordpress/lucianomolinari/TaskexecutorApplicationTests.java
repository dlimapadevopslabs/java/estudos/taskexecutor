package com.wordpress.lucianomolinari;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.wordpress.lucianomolinari.taskexecutor.unrelatedtask.SimpleTask;
import com.wordpress.lucianomolinari.taskexecutor.unrelatedtask.SimpleTaskExecutor;
import com.wordpress.lucianomolinari.taskexecutor.unrelatedtask.SimpleTaskExecutorParallel;
import com.wordpress.lucianomolinari.taskexecutor.unrelatedtask.SimpleTaskExecutorSequential;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TaskexecutorApplicationTests {
	private final Logger logger = Logger.getLogger(TaskexecutorApplicationTests.class);

	@Test
	public void testSequential() {
		logger.debug("--------------------------------------------");
		logger.debug("Excuting tasks in a sequential manner...");
		SimpleTaskExecutor executor = new SimpleTaskExecutorSequential();
		for (SimpleTask task: getListToBeExecuted()) {
			executor.execute(task);
		}
	}
	
	@Test
	public void testPrallel() {
		logger.debug("--------------------------------------------");
		logger.debug("Excuting tasks in a parallel manner...");
		SimpleTaskExecutor executor = new SimpleTaskExecutorParallel();
		
		for (SimpleTask task: getListToBeExecuted()) {
			executor.execute(task);
		}
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	
	private List<SimpleTask> getListToBeExecuted() {
		List<SimpleTask> tasks = new ArrayList<SimpleTask>();
		for (int index = 1; index <= 50; index++) {
			tasks.add(new SimpleTask("Message " + index));			
		}
		return tasks;
	}

}
