package com.wordpress.lucianomolinari.taskexecutor.unrelatedtask;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SimpleTaskExecutorParallel implements SimpleTaskExecutor {
	
	/*
	 * Pode ser executado objetos Runnable ou Callable
	 * Runnable: executa tarefas void
	 * Callable: retorna um valor
	 */
	private final ExecutorService executor = Executors.newFixedThreadPool(10);
	
	@Override
	public void execute(final SimpleTask simpleTask) {
		executor.execute(new Runnable() {
			
			@Override
			public void run() {
				simpleTask.execute();
			}
		});
	}
}
