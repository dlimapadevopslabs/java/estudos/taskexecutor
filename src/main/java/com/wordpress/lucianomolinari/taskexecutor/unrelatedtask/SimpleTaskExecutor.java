package com.wordpress.lucianomolinari.taskexecutor.unrelatedtask;

/*
 * interface a ser usada pelo executors sequencial e paralelo.
 */
public interface SimpleTaskExecutor {
	void execute(final SimpleTask simpleTask);
}
