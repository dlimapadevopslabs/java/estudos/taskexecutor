package com.wordpress.lucianomolinari.taskexecutor.unrelatedtask;

public class SimpleTaskExecutorSequential implements SimpleTaskExecutor {
	
	@Override
	public void execute(final SimpleTask simpleTask) {
		simpleTask.execute();
	}
}
